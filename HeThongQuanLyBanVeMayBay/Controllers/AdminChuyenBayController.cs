﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HeThongQuanLyBanVeMayBay.Models;

namespace HeThongQuanLyBanVeMayBay.Controllers
{
    public class AdminChuyenBayController : Controller
    {
        private HTQLBVMBver1 db = new HTQLBVMBver1();

        // GET: AdminChuyenBay
        public ActionResult Index()
        {
            if (SESSION.MaGp.Equals("Admin") || SESSION.MaGp.Equals("Nhân viên quản lý chuyến bay") || SESSION.MaGp.Equals("Nhân viên bán vé"))
            {
                return View(db.CHUYENBAYs.ToList());
            }
            else
            {
                TempData["CanhBao1"] = "Xin lỗi !! bạn không được cấp quyền vào mục này !!!";
                return RedirectToAction("TrangChuAdmin", "Admin");
            }
        }

        // GET: AdminChuyenBay/Details/5
        public ActionResult Details(string id)
        {
            if (SESSION.MaGp.Equals("Admin") || SESSION.MaGp.Equals("Nhân viên quản lý chuyến bay"))
            {

                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                CHUYENBAY cHUYENBAY = db.CHUYENBAYs.Find(id);
                if (cHUYENBAY == null)
                {
                    return HttpNotFound();
                }
                return View(cHUYENBAY);
            }
            else
            {
                TempData["CanhBao2"] = "Xin lỗi !! bạn không được cấp quyền vào mục này !!!";
                return RedirectToAction("Index", "AdminChuyenBay");
            }
        }

        // GET: AdminChuyenBay/Create
        public ActionResult Create()
        {
            if (SESSION.MaGp.Equals("Admin") || SESSION.MaGp.Equals("Nhân viên quản lý chuyến bay"))
            {
                return View();
            }
            else
            {
                TempData["CanhBao3"] = "Xin lỗi !! bạn không được cấp quyền để thêm mới chuyến bay !!!";
                return RedirectToAction("Index", "AdminChuyenBay");
            }
        }

        // POST: AdminChuyenBay/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MaChuyenBay,SanBayDi,SanBayDen,ThoiGianBay,ThoiGianDen,TrinhTrang,SoGheNguoiLon,SoGheTreEm,SoGheEmBe")] CHUYENBAY cHUYENBAY)
        {
            
                if (ModelState.IsValid)
                {
                    db.CHUYENBAYs.Add(cHUYENBAY);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                return View(cHUYENBAY);
           
        }

        // GET: AdminChuyenBay/Edit/5
        public ActionResult Edit(string id)
        {
            if (SESSION.MaGp.Equals("Admin") || SESSION.MaGp.Equals("Nhân viên quản lý chuyến bay"))
            {

                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                CHUYENBAY cHUYENBAY = db.CHUYENBAYs.Find(id);
                if (cHUYENBAY == null)
                {
                    return HttpNotFound();
                }
                return View(cHUYENBAY);
            }
            else
            {
                TempData["CanhBao4"] = "Xin lỗi !! bạn không được cấp quyền chỉnh sửa chuyến bay !!!";
                return RedirectToAction("Index", "AdminChuyenBay");
            }

        }

        // POST: AdminChuyenBay/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MaChuyenBay,SanBayDi,SanBayDen,ThoiGianBay,ThoiGianDen,TrinhTrang,SoGheNguoiLon,SoGheTreEm,SoGheEmBe")] CHUYENBAY cHUYENBAY)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cHUYENBAY).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cHUYENBAY);
        }

        // GET: AdminChuyenBay/Delete/5
        public ActionResult Delete(string id)
        {
            if (SESSION.MaGp.Equals("Admin") || SESSION.MaGp.Equals("Nhân viên quản lý chuyến bay"))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                CHUYENBAY cHUYENBAY = db.CHUYENBAYs.Find(id);
                if (cHUYENBAY == null)
                {
                    return HttpNotFound();
                }
                return View(cHUYENBAY);
            }
            else
            {
                TempData["CanhBao5"] = "Xin lỗi !! bạn không được cấp quyền để xóa chuyến bay !!!";
                return RedirectToAction("Index", "AdminChuyenBay");
            }
        }

        // POST: AdminChuyenBay/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            CHUYENBAY cHUYENBAY = db.CHUYENBAYs.Find(id);
            if(db.VEs != null)
            {
                TempData["Tam"] = "Không được xóa chuyến bay vì mục quản lý vé vẫn còn dữ liệu có thể liên kết, hãy liên hệ với admin thực hiện tác vụ này";
                return RedirectToAction("Index");
            }
            db.CHUYENBAYs.Remove(cHUYENBAY);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
