﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HeThongQuanLyBanVeMayBay.Models;

namespace HeThongQuanLyBanVeMayBay.Controllers
{
    public class AdminKhachHangController : Controller
    {
        private HTQLBVMBver1 db = new HTQLBVMBver1();

        // GET: AdminKhachHang
        public ActionResult Index()
        {
            if (SESSION.MaGp.Equals("Admin") || SESSION.MaGp.Equals("Nhân viên bán vé"))
            {
                return View(db.KHACHHANGs.ToList());
            }
            else
            {
                TempData["CanhBao1"] = "Xin lỗi !! bạn không được cấp quyền vào mục này !!!";
                return RedirectToAction("TrangChuAdmin", "Admin");
            }
        }

        // GET: AdminKhachHang/Details/5
        public ActionResult Details(int? id)
        {
            if (SESSION.MaGp.Equals("Admin") || SESSION.MaGp.Equals("Nhân viên bán vé"))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                KHACHHANG kHACHHANG = db.KHACHHANGs.Find(id);
                if (kHACHHANG == null)
                {
                    return HttpNotFound();
                }
                return View(kHACHHANG);
            }
            else
            {
                TempData["CanhBao2"] = "Xin lỗi !! bạn không được cấp quyền vào mục này !!!";
                return RedirectToAction("Index", "AdminKhachHang");
            }
        }

        // GET: AdminKhachHang/Create
        public ActionResult Create()
        {
            if (SESSION.MaGp.Equals("Admin") || SESSION.MaGp.Equals("Nhân viên bán vé"))
            {
                return View();
            }
            else
            {
                TempData["CanhBao3"] = "Xin lỗi !! bạn không được cấp quyền để thêm mới khách hàng !!!";
                return RedirectToAction("Index", "AdminKhachHang");
            }
           
        }

        // POST: AdminKhachHang/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MaKH,TenKH,TenDangNhap,MatKhau,NgaySinh,SDT,CMND,Email,DeleteFlag,DeleteTime")] KHACHHANG kHACHHANG)
        {
            if (ModelState.IsValid)
            {
                db.KHACHHANGs.Add(kHACHHANG);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(kHACHHANG);
        }

        // GET: AdminKhachHang/Edit/5
        public ActionResult Edit(int? id)
        {
            if (SESSION.MaGp.Equals("Admin") || SESSION.MaGp.Equals("Nhân viên bán vé"))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                KHACHHANG kHACHHANG = db.KHACHHANGs.Find(id);
                if (kHACHHANG == null)
                {
                    return HttpNotFound();
                }
                return View(kHACHHANG);
            }
            else
            {
                TempData["CanhBao4"] = "Xin lỗi !! bạn không được cấp quyền chỉnh sửa khách hàng !!!";
                return RedirectToAction("Index", "AdminKhachHang");
            }
            
        }

        // POST: AdminKhachHang/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MaKH,TenKH,TenDangNhap,MatKhau,NgaySinh,SDT,CMND,Email,DeleteFlag,DeleteTime")] KHACHHANG kHACHHANG)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kHACHHANG).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(kHACHHANG);
        }

        // GET: AdminKhachHang/Delete/5
        public ActionResult Delete(int? id)
        {
            if (SESSION.MaGp.Equals("Admin") || SESSION.MaGp.Equals("Nhân viên bán vé"))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                KHACHHANG kHACHHANG = db.KHACHHANGs.Find(id);
                if (kHACHHANG == null)
                {
                    return HttpNotFound();
                }
                return View(kHACHHANG);
            }
            else
            {
                TempData["CanhBao5"] = "Xin lỗi !! bạn không được cấp quyền để xóa khách hàng !!!";
                return RedirectToAction("Index", "AdminKhachHang");
            }
        }

        // POST: AdminKhachHang/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            KHACHHANG kHACHHANG = db.KHACHHANGs.Find(id);
            db.KHACHHANGs.Remove(kHACHHANG);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
