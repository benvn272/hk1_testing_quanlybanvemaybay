﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HeThongQuanLyBanVeMayBay.Models;

namespace HeThongQuanLyBanVeMayBay.Controllers
{
    public class AdminChiTietDonDatVeController : Controller
    {
        private HTQLBVMBver1 db = new HTQLBVMBver1();

        // GET: AdminChiTietDonDatVe
        public ActionResult Index()
        {
            if (SESSION.MaGp.Equals("Admin") || SESSION.MaGp.Equals("Nhân viên bán vé"))
            {
                var cT_DDV = db.CT_DDV.Include(c => c.DONDATVE).Include(c => c.VE);
                return View(cT_DDV.ToList());
            }
            else
            {
                TempData["CanhBao1"] = "Xin lỗi !! bạn không được cấp quyền vào mục này !!!";
                return RedirectToAction("TrangChuAdmin", "Admin");
            }
        }

        public ActionResult ChuyenDonHang(int? DonHangID)
        {
            if (SESSION.MaGp.Equals("Admin") || SESSION.MaGp.Equals("Nhân viên bán vé"))
            {
                if (DonHangID == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                CT_DDV ctddv = db.CT_DDV.SingleOrDefault(n => n.MaDDV == DonHangID);
                if (ctddv == null)
                {
                    return HttpNotFound();
                }
                var ldh = db.CT_DDV.Include(n => n.DONDATVE);
                ldh = ldh.Where(m => m.MaDDV == DonHangID);
                VE ve = db.VEs.SingleOrDefault(n => n.MaVe == ctddv.MaVe);

                if (ve.SoLuong <= 0)
                {
                    TempData["BaoLoi"] = "Không đủ số lượng vé, hãy liên lạc và thông báo cho khách hàng";
                    return RedirectToAction("Index");
                }
                else
                {
                    ctddv.DaThanhToan = 1;
                    ve.SoLuong = ve.SoLuong - ctddv.SoLuongDat;
                    TempData["ThanhCong"] = "Đã duyệt và thanh toán thành công !!";
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            else
            {
                TempData["CanhBao2"] = "Xin lỗi !! bạn không được cấp quyền vào mục này !!!";
                return RedirectToAction("TrangChuAdmin", "Admin");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
