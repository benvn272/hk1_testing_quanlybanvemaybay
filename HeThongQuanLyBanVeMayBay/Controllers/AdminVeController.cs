﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HeThongQuanLyBanVeMayBay.Models;

namespace HeThongQuanLyBanVeMayBay.Controllers
{
    public class AdminVeController : Controller
    {
        private HTQLBVMBver1 db = new HTQLBVMBver1();

        // GET: AdminVe
        public ActionResult Index()
        {
            if(SESSION.MaGp.Equals("Admin")|| SESSION.MaGp.Equals("Nhân viên bán vé"))
            {
                var vEs = db.VEs.Include(v => v.CHUYENBAY).Include(v => v.LOAIVE);
                return View(vEs.ToList());
            }
            else
            {
                TempData["CanhBao1"] = "Xin lỗi !! bạn không được cấp quyền vào mục này !!!";
                return RedirectToAction("TrangChuAdmin", "Admin");
            }
        }

        // GET: AdminVe/Details/5
        public ActionResult Details(int? id)
        {
            if (SESSION.MaGp.Equals("Admin") || SESSION.MaGp.Equals("Nhân viên bán vé"))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                VE vE = db.VEs.Find(id);
                if (vE == null)
                {
                    return HttpNotFound();
                }
                return View(vE);
            }
            else
            {
                TempData["CanhBao2"] = "Xin lỗi !! bạn không được cấp quyền vào mục này !!!";
                return RedirectToAction("Index", "AdminVe");
            }
           
        }

        // GET: AdminVe/Create
        public ActionResult Create()
        {
            if (SESSION.MaGp.Equals("Admin") || SESSION.MaGp.Equals("Nhân viên bán vé"))
            {
                ViewBag.MaChuyenBay = new SelectList(db.CHUYENBAYs, "MaChuyenBay", "MaChuyenBay");
                ViewBag.MaLoaiVe = new SelectList(db.LOAIVEs, "MaLoaiVe", "TenLoaiVe");
                return View();
            }
            else
            {
                TempData["CanhBao3"] = "Xin lỗi !! bạn không được cấp quyền để thêm mới vé bay !!!";
                return RedirectToAction("Index", "AdminVe");
            }
            
        }

        // POST: AdminVe/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MaVe,MaChuyenBay,MaLoaiVe,SoLuong,GiaVe,TrangThai,DeleteFlag,DeleteTime")] VE vE)
        {
            if (ModelState.IsValid)
            {
                db.VEs.Add(vE);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.MaChuyenBay = new SelectList(db.CHUYENBAYs, "MaChuyenBay", "SanBayDi", vE.MaChuyenBay);
            ViewBag.MaLoaiVe = new SelectList(db.LOAIVEs, "MaLoaiVe", "TenLoaiVe", vE.MaLoaiVe);
            return View(vE);
        }

        // GET: AdminVe/Edit/5
        public ActionResult Edit(int? id)
        {
            if (SESSION.MaGp.Equals("Admin") || SESSION.MaGp.Equals("Nhân viên bán vé"))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                VE vE = db.VEs.Find(id);
                if (vE == null)
                {
                    return HttpNotFound();
                }
                ViewBag.MaChuyenBay = new SelectList(db.CHUYENBAYs, "MaChuyenBay", "MaChuyenBay", vE.MaChuyenBay);
                ViewBag.MaLoaiVe = new SelectList(db.LOAIVEs, "MaLoaiVe", "TenLoaiVe", vE.MaLoaiVe);
                return View(vE);
            }
            else
            {
                TempData["CanhBao4"] = "Xin lỗi !! bạn không được cấp quyền chỉnh sửa vé bay !!!";
                return RedirectToAction("Index", "AdminVe");
            }
            
        }

        // POST: AdminVe/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MaVe,MaChuyenBay,MaLoaiVe,SoLuong,GiaVe,TrangThai,DeleteFlag,DeleteTime")] VE vE)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vE).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MaChuyenBay = new SelectList(db.CHUYENBAYs, "MaChuyenBay", "SanBayDi", vE.MaChuyenBay);
            ViewBag.MaLoaiVe = new SelectList(db.LOAIVEs, "MaLoaiVe", "TenLoaiVe", vE.MaLoaiVe);
            return View(vE);
        }

        // GET: AdminVe/Delete/5
        public ActionResult Delete(int? id)
        {
            if (SESSION.MaGp.Equals("Admin") || SESSION.MaGp.Equals("Nhân viên bán vé"))
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                VE vE = db.VEs.Find(id);
                if (vE == null)
                {
                    return HttpNotFound();
                }
                return View(vE);
            }
            else
            {
                TempData["CanhBao5"] = "Xin lỗi !! bạn không được cấp quyền để xóa vé bay !!!";
                return RedirectToAction("Index", "AdminVe");
            }
            
        }

        // POST: AdminVe/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            VE vE = db.VEs.Find(id);
            if (db.DONDATVEs != null && db.CT_DDV != null)
            {
                TempData["Tam"] = "Không được xóa vé vì mục quản lý đơn đặt vé vẫn còn dữ liệu có thể liên kết, hãy liên hệ với admin thực hiện tác vụ này";
                return RedirectToAction("Index");
            }
            db.VEs.Remove(vE);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
