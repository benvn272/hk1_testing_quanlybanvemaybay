﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HeThongQuanLyBanVeMayBay.Models;

namespace HeThongQuanLyBanVeMayBay.Controllers
{
    public class SuaLoaiVeOnlineController : Controller
    {
        private HTQLBVMBver1 db = new HTQLBVMBver1();

        // GET: SuaLoaiVeOnline
        public ActionResult Index()
        {
            return View(db.LOAIVEs.ToList());
        }

        // GET: SuaLoaiVeOnline/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LOAIVE lOAIVE = db.LOAIVEs.Find(id);
            if (lOAIVE == null)
            {
                return HttpNotFound();
            }
            return View(lOAIVE);
        }

        // GET: SuaLoaiVeOnline/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SuaLoaiVeOnline/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MaLoaiVe,TenLoaiVe")] LOAIVE lOAIVE)
        {
            if (ModelState.IsValid)
            {
                db.LOAIVEs.Add(lOAIVE);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(lOAIVE);
        }

        // GET: SuaLoaiVeOnline/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LOAIVE lOAIVE = db.LOAIVEs.Find(id);
            if (lOAIVE == null)
            {
                return HttpNotFound();
            }
            return View(lOAIVE);
        }

        // POST: SuaLoaiVeOnline/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MaLoaiVe,TenLoaiVe")] LOAIVE lOAIVE)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lOAIVE).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(lOAIVE);
        }

        // GET: SuaLoaiVeOnline/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LOAIVE lOAIVE = db.LOAIVEs.Find(id);
            if (lOAIVE == null)
            {
                return HttpNotFound();
            }
            return View(lOAIVE);
        }

        // POST: SuaLoaiVeOnline/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            LOAIVE lOAIVE = db.LOAIVEs.Find(id);
            db.LOAIVEs.Remove(lOAIVE);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
