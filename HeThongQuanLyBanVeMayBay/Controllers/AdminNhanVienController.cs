﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HeThongQuanLyBanVeMayBay.Models;

namespace HeThongQuanLyBanVeMayBay.Controllers
{
    public class AdminNhanVienController : Controller
    {
        private HTQLBVMBver1 db = new HTQLBVMBver1();

        // GET: AdminNhanVien
        public ActionResult Index()
        {
            if(SESSION.MaGp.Equals("Admin"))
            {
                var nHANVIENs = db.NHANVIENs.Include(n => n.CHUCVU);
                return View(nHANVIENs.ToList());
            }
            else
            {
                TempData["CanhBao1"] = "Xin lỗi !! bạn không được cấp quyền vào mục này !!!";
                return RedirectToAction("TrangChuAdmin", "Admin");
            }
        }

        // GET: AdminNhanVien/Details/5
        public ActionResult Details(int? id)
        {
            if (SESSION.MaGp.Equals("Admin"))
            {

                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                NHANVIEN nHANVIEN = db.NHANVIENs.Find(id);
                if (nHANVIEN == null)
                {
                    return HttpNotFound();
                }
                return View(nHANVIEN);
            }
            else
            {
                TempData["CanhBao2"] = "Hệ thống phát hiện bạn đang cố tình truy cập trái phép, đề nghị thực hiện đúng chức năng của mình !!!";
                return RedirectToAction("TrangChuAdmin", "Admin");
            }


        }

        // GET: AdminNhanVien/Create
        public ActionResult Create()
        {
            if (SESSION.MaGp.Equals("Admin"))
            {
                ViewBag.MaChucVu = new SelectList(db.CHUCVUs, "MaChucVu", "TenChucVu");
                return View();
            }
            else
            {
                TempData["CanhBao2"] = "Hệ thống phát hiện bạn đang cố tình truy cập trái phép, đề nghị thực hiện đúng chức năng của mình !!!";
                return RedirectToAction("TrangChuAdmin", "Admin");
            }
        }

        // POST: AdminNhanVien/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MaNV,MaChucVu,TenNV,TenDangNhap,MatKhau,NgaySinh,DiaChi,Email,SDT")] NHANVIEN nHANVIEN)
        {
            if (ModelState.IsValid)
            {
                db.NHANVIENs.Add(nHANVIEN);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.MaChucVu = new SelectList(db.CHUCVUs, "MaChucVu", "TenChucVu", nHANVIEN.MaChucVu);
            return View(nHANVIEN);
        }

        // GET: AdminNhanVien/Edit/5
        public ActionResult Edit(int? id)
        {
            if (SESSION.MaGp.Equals("Admin"))
            {

                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                NHANVIEN nHANVIEN = db.NHANVIENs.Find(id);
                if (nHANVIEN == null)
                {
                    return HttpNotFound();
                }
                ViewBag.MaChucVu = new SelectList(db.CHUCVUs, "MaChucVu", "TenChucVu", nHANVIEN.MaChucVu);
                return View(nHANVIEN);
            }
            else
            {
                TempData["CanhBao2"] = "Hệ thống phát hiện bạn đang cố tình truy cập trái phép, đề nghị thực hiện đúng chức năng của mình !!!";
                return RedirectToAction("TrangChuAdmin", "Admin");
            }

        }

        // POST: AdminNhanVien/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MaNV,MaChucVu,TenNV,TenDangNhap,MatKhau,NgaySinh,DiaChi,Email,SDT")] NHANVIEN nHANVIEN)
        {
            if (ModelState.IsValid)
            {
                db.Entry(nHANVIEN).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MaChucVu = new SelectList(db.CHUCVUs, "MaChucVu", "TenChucVu", nHANVIEN.MaChucVu);
            return View(nHANVIEN);
        }

        // GET: AdminNhanVien/Delete/5
        public ActionResult Delete(int? id)
        {
            if (SESSION.MaGp.Equals("Admin"))
            {

                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                NHANVIEN nHANVIEN = db.NHANVIENs.Find(id);
                if (nHANVIEN == null)
                {
                    return HttpNotFound();
                }
                return View(nHANVIEN);
            }
            else
            {
                TempData["CanhBao2"] = "Hệ thống phát hiện bạn đang cố tình truy cập trái phép, đề nghị thực hiện đúng chức năng của mình !!!";
                return RedirectToAction("TrangChuAdmin", "Admin");
            }

        }

        // POST: AdminNhanVien/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            NHANVIEN nHANVIEN = db.NHANVIENs.Find(id);
            db.NHANVIENs.Remove(nHANVIEN);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
