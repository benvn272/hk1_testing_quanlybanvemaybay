﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using HeThongQuanLyBanVeMayBay.Models;

namespace HeThongQuanLyBanVeMayBay.Controllers
{
    // GET: Admin/MainLogin
    public class AdminController : Controller
    {
        HTQLBVMBver1 db = new HTQLBVMBver1();
        [HttpGet]
        public ActionResult Dangnhap()
        {
            return View();
        }
        public ActionResult DangNhap(FormCollection collection)
        {

            var TenDN = collection["tenDN"];
            var MatKhau = collection["matkhau"];
            if (String.IsNullOrEmpty(TenDN) && String.IsNullOrEmpty(MatKhau))
            {
                ViewData["loi0"] = "Vui lòng điền vào 'Tên đăng nhập' và 'Mật khẩu'";
            }
            else if (String.IsNullOrEmpty(TenDN))
            {
                ViewData["loi1"] = "Tên đăng nhập không được để trống";
            }
            else if (String.IsNullOrEmpty(MatKhau))
            {
                ViewData["loi2"] = "Mật khẩu không được để trống";
            }
            else
            {
                //String temp = MaHoaMD5.convertMD5(MatKhau);
                NHANVIEN kh = db.NHANVIENs.SingleOrDefault(n => n.TenDangNhap == TenDN && n.MatKhau == MatKhau);
                if (kh != null)
                {
                    ViewBag.ThongBao = "Đăng nhập thành công";
                    //Session["Taikhoan"] = kh.CustID;
                    SESSION.MaUser = kh.TenDangNhap;
                    //SESSION.TEN_USER = user.TenNhanVien;
                    SESSION.MaGp = kh.CHUCVU.TenChucVu;
                    //Session["TenNV"] = user.TenNV;
                    SESSION.TenUser = kh.TenNV;
                    return RedirectToAction("TrangChuAdmin", "Admin");
                }
                else
                    ViewBag.ThongBao = "Tên đăng nhập hoặc mật khẩu không đúng";
            }
            return View();
        }
        public ActionResult TrangChuAdmin()
        {
            if (SESSION.TenUser == null)
            {
                TempData["CanhBao"] = "Xin lỗi !! bạn phải đăng nhập để vào hệ thống nhé !!!";
                return RedirectToAction("Dangnhap", "Admin");
            }
            return View();
        }
        public ActionResult Dangxuat()
        {
            //Session["TenNV"] = null;
            SESSION.MaUser = null;
            SESSION.TenUser = null;
            SESSION.MaGp = null;
            return RedirectToAction("XemChuyenBay", "KhachHang");
        }
    }
}