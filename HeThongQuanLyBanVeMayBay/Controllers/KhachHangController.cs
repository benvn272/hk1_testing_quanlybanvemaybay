﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HeThongQuanLyBanVeMayBay.Models;

namespace HeThongQuanLyBanVeMayBay.Controllers
{
    public class KhachHangController : Controller
    {
        // GET: KhachHang
        public ActionResult Index()
        {
            
            return View();
        }
        private HTQLBVMBver1 db = new HTQLBVMBver1();

        // GET: CHUYENBAYs
        public ActionResult XemChuyenBay()
        {
            return View(db.CHUYENBAYs.ToList());
        }
        // GET: KHACHHANGs/Create
        public ActionResult DangKy()
        {
            return View();
        }

        // POST: KHACHHANGs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DangKy([Bind(Include = "MaKH,TenKH,TenDangNhap,MatKhau,NgaySinh,SDT,CMND,Email,DeleteFlag,DeleteTime")] KHACHHANG kHACHHANG)
        {
            var use = db.KHACHHANGs.SingleOrDefault(n => n.TenDangNhap == kHACHHANG.TenDangNhap);
            var use2 = db.KHACHHANGs.SingleOrDefault(n => n.Email == kHACHHANG.Email);

            if (use != null)
            {
                TempData["tendangnhap"] = "Tên đang nhập đã đươc đăng ký";
                return RedirectToAction("DangKy", "KhachHang");
            }
            if(use2 != null)
            {
                TempData["email"] = "Email đã đươc đăng ký";
                return RedirectToAction("DangKy", "KhachHang");

            }
            if (ModelState.IsValid)
            {
                db.KHACHHANGs.Add(kHACHHANG);
                db.SaveChanges();
                return RedirectToAction("DangNhap", "KhachHang");
            }

            return View(kHACHHANG);
        }
        [HttpGet]
        public ActionResult DangNhap()
        {
            return View();
        }
        public ActionResult DangNhap(FormCollection collection)
        {
            HTQLBVMBver1 db = new HTQLBVMBver1();
            var TenDN = collection["tenDN"];
            var MatKhau = collection["matkhau"];
            if (String.IsNullOrEmpty(TenDN) && String.IsNullOrEmpty(MatKhau))
            {
                ViewData["loi3"] = "Vui lòng điền vào 'Tên đăng nhập' và 'Mật khẩu'";
            }
            else if (String.IsNullOrEmpty(TenDN))
            {
                ViewData["loi4"] = "Tên đăng nhập không được để trống";
            }
            else if (String.IsNullOrEmpty(MatKhau))
            {
                ViewData["loi5"] = "Mật khẩu không được để trống";
            }
            else
            {
                //String temp = MaHoaMD5.convertMD5(MatKhau);
                KHACHHANG kh = db.KHACHHANGs.SingleOrDefault(n => n.TenDangNhap == TenDN && n.MatKhau == MatKhau);
                if (kh != null)
                {
                    ViewBag.ThongBao = "Đăng nhập thành công";
                    //Session["Taikhoan"] = kh.CustID;
                    Session["Taikhoan"] = kh.MaKH;
                    //SESSION.TEN_USER = user.TenNhanVien;
                    SESSION.TenUser = kh.TenKH;
                    return RedirectToAction("TrangChuLogged", "KhachHang");
                }
                else
                    ViewBag.ThongBao = "Tên đăng nhập hoặc mật khẩu không đúng";
            }
            return View();
        }

        //---------------------------------------khu sau khi logged--------------------
        public ActionResult TrangChuLogged()
        {
            if (SESSION.TenUser == null)
            {
                return RedirectToAction("DangNhap", "KhachHang");
            }
            return View();
        }

        public ActionResult DangXuat()
        {
            //Session["TenNV"] = null;

            SESSION.TenUser = null;

            return RedirectToAction("DangNhap", "KhachHang");
        }
        //---------------------Thiết kế chọn vé và đặt vé-----------------------------
        private List<VE> LayVeMoi(int count)
        {
            return db.VEs.OrderByDescending(a => a.MaVe).Take(count).ToList();
        }

        public ActionResult DanhSachVeLogged()
        {
            var layvemoi = LayVeMoi(20);
            return View(layvemoi);
        }

        public ActionResult ChiTietVeBayLogged(int id)
        {
            var ve = from p in db.VEs
                     where p.MaVe == id
                     select p;
            return View(ve.Single());
        }
//--------------làm giỏ hàng tại đây----------------------------------------
        public List<Giohang> LayGioHang()
        {
            List<Giohang> lstGioHang = Session["Giohang"] as List<Giohang>;
            if (lstGioHang == null)
            {
                //nếu giỏ hàng chưa tồn tại thì khởi tạo list giỏ hàng
                lstGioHang = new List<Giohang>();
                Session["Giohang"] = lstGioHang;
            }
            return lstGioHang;
        }

        public ActionResult ThemGioHang(int MaVe, string strURL)
        {
            //lấy ra session giỏ hàng
            List<Giohang> lstGioHang = LayGioHang();
            //kiểm tra xem phụ kiện có trong Session["GioHang"] hay chưa ?
            Giohang sanpham = lstGioHang.Find(n => n.MaVe == MaVe);
            if (sanpham == null)
            {
                sanpham = new Giohang(MaVe);
                lstGioHang.Add(sanpham);
                //return Redirect(strURL);
                TempData["ThongBaoSttGioHang"] = "Truy cập 'Xem vé trong giỏ' để xem Vé đã đặt";
                return RedirectToAction("DanhSachVeLogged", "KhachHang");
            }
            else if (sanpham != null)
            {
                TempData["ThongBaoSttGioHang1"] = "Vé này đã đặt rồi";
                return RedirectToAction("DanhSachVeLogged", "KhachHang");
            }
            else
            {
                sanpham.SoLuong++;
                return Redirect(strURL);
            }
        }

        //Tính tổng số lượng hàng
        private int TongSoLuong()
        {
            int iTongSoLuong = 0;
            List<Giohang> lstGioHang = Session["Giohang"] as List<Giohang>;
            if (lstGioHang != null)
            {
                iTongSoLuong = lstGioHang.Sum(n => n.SoLuong);
            }
            return iTongSoLuong;
        }

        //Tính tổng tiền
        private double TongTien()
        {
            double iTongTien = 0;
            List<Giohang> lstGioHang = Session["Giohang"] as List<Giohang>;
            if (lstGioHang != null)
            {
                iTongTien = lstGioHang.Sum(n => n.dThanhTien);
            }
            return iTongTien;
        }


        //Xây dựng trang giỏ hàng
        public ActionResult GioHang()
        {
            List<Giohang> lstGioHang = LayGioHang();
            if (lstGioHang.Count == 0)
            {
                TempData["GioHangError"] = "Giỏ hàng đang trống, hãy tiến hành đặt hàng";
                return RedirectToAction("DanhSachVeLogged", "KhachHang");
            }
            var list = db.VEs.Where(n => n.SoLuong != 0).ToList();
            ViewBag.TongSoLuong = TongSoLuong();
            ViewBag.TongTien = TongTien();
            return View(list.ToList());
        }

        public ActionResult GioHangPartial()
        {
            ViewBag.TongSoLuong = TongSoLuong();
            ViewBag.TongTien = TongTien();
            return PartialView();
        }

        public ActionResult XoaGioHang(int iMaVe)
        {
            List<Giohang> lstGioHang = LayGioHang();

            Giohang sanpham = lstGioHang.SingleOrDefault(n => n.MaVe == iMaVe);

            if (sanpham != null)
            {
                lstGioHang.RemoveAll(n => n.MaVe == iMaVe);
                return RedirectToAction("GioHang");
            }
            if (lstGioHang.Count == 0)
            {
                return RedirectToAction("TrangChuLogged", "KhachHang");
            }
            return RedirectToAction("GioHang");
        }
        public ActionResult CapNhatGioHang(int iMaVe, FormCollection f)
        {
            List<Giohang> lstGioHang = LayGioHang();
            Giohang sanpham = lstGioHang.SingleOrDefault(n => n.MaVe == iMaVe);
            if (sanpham != null)
            {
                sanpham.SoLuong = int.Parse(f["txtSoluong"].ToString());
            }
            return RedirectToAction("Giohang");
        }
        public ActionResult XoaTatCaGioHang()
        {
            List<Giohang> lstGioHang = LayGioHang();
            lstGioHang.Clear();
            return RedirectToAction("GioHang", "KhachHang");
        }

//--------------------------Xây dựng trang đặt hàng-----------------------------
        [HttpGet]
        public ActionResult DatHang()
        {

            //kiểm tra đăng nhập
            if (Session["Taikhoan"] == null || Session["Taikhoan"].ToString() == "")
            {
                return RedirectToAction("KhachHangDangNhap", "KhachHang");
            }
            if (Session["Giohang"] == null)
            {
                return RedirectToAction("TrangChuLogged", "KhachHang");
            }
            //lấy giỏ hàng từ session

            List<Giohang> lstGioHang = LayGioHang();
            ViewBag.TongSoLuong = TongSoLuong();
            ViewBag.TongTien = TongTien();

            return View(lstGioHang);
        }
        public ActionResult DatHang(FormCollection collection)
        {
            List<Giohang> gh = LayGioHang();
            foreach (var item in gh)
            {
                DONDATVE ddh = new DONDATVE();
                ddh.MaKH = ((int)Session["Taikhoan"]);
                ddh.ThanhTien = (decimal)item.dThanhTien;
                ddh.NgayMuaVe = DateTime.Now;
                
                db.DONDATVEs.Add(ddh);

                CT_DDV ctdh = new CT_DDV();
                ctdh.SoLuongDat = item.SoLuong;
                ctdh.MaDDV = ddh.MaDDV;
                ctdh.MaVe = item.MaVe;
                ctdh.DaThanhToan = 0;
                ctdh.GiaVe = (decimal)item.GiaVe;
                db.CT_DDV.Add(ctdh);
            }
            db.SaveChanges();
            Session["Giohang"] = null;
            return RedirectToAction("XacNhanDonHang", "KhachHang");
        }
        public ActionResult XacNhanDonHang()
        {
            return View();
        }
        //-----------------------------------------------------------------------
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}