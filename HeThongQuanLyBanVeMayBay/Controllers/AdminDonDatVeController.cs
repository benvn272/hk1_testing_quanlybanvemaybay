﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HeThongQuanLyBanVeMayBay.Models;

namespace HeThongQuanLyBanVeMayBay.Controllers
{
    public class AdminDonDatVeController : Controller
    {
        private HTQLBVMBver1 db = new HTQLBVMBver1();

        // GET: AdminDonDatVe
        public ActionResult Index()
        {
            if (SESSION.MaGp.Equals("Admin") || SESSION.MaGp.Equals("Nhân viên bán vé"))
            {
                var dONDATVEs = db.DONDATVEs.Include(d => d.KHACHHANG).Include(d => d.KHUYENMAI);
                return View(dONDATVEs.ToList());
            }
            else
            {
                TempData["CanhBao1"] = "Xin lỗi !! bạn không được cấp quyền vào mục này !!!";
                return RedirectToAction("TrangChuAdmin", "Admin");
            }
        }
        //-----------------------------------lam duyet don dat hang--------------------------------------------------------------
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
