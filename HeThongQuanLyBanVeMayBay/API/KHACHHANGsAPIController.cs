﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using HeThongQuanLyBanVeMayBay.Models;

namespace HeThongQuanLyBanVeMayBay.API
{
    public class KHACHHANGsAPIController : ApiController
    {
        private HTQLBVMBver1 db = new HTQLBVMBver1();

        // GET: api/KHACHHANGsAPI
        public IQueryable<KHACHHANG> GetKHACHHANGs()
        {
            return db.KHACHHANGs;
        }

        // GET: api/KHACHHANGsAPI/5
        [ResponseType(typeof(KHACHHANG))]
        public IHttpActionResult GetKHACHHANG(int id)
        {
            KHACHHANG kHACHHANG = db.KHACHHANGs.Find(id);
            if (kHACHHANG == null)
            {
                return NotFound();
            }

            return Ok(kHACHHANG);
        }

        // PUT: api/KHACHHANGsAPI/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutKHACHHANG(int id, KHACHHANG kHACHHANG)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != kHACHHANG.MaKH)
            {
                return BadRequest();
            }

            db.Entry(kHACHHANG).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!KHACHHANGExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/KHACHHANGsAPI
        [ResponseType(typeof(KHACHHANG))]
        public IHttpActionResult PostKHACHHANG(KHACHHANG kHACHHANG)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.KHACHHANGs.Add(kHACHHANG);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = kHACHHANG.MaKH }, kHACHHANG);
        }

        // DELETE: api/KHACHHANGsAPI/5
        [ResponseType(typeof(KHACHHANG))]
        public IHttpActionResult DeleteKHACHHANG(int id)
        {
            KHACHHANG kHACHHANG = db.KHACHHANGs.Find(id);
            if (kHACHHANG == null)
            {
                return NotFound();
            }

            db.KHACHHANGs.Remove(kHACHHANG);
            db.SaveChanges();

            return Ok(kHACHHANG);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool KHACHHANGExists(int id)
        {
            return db.KHACHHANGs.Count(e => e.MaKH == id) > 0;
        }
    }
}