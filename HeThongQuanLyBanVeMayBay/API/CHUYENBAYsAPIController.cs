﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using HeThongQuanLyBanVeMayBay.Models;

namespace HeThongQuanLyBanVeMayBay.API
{
    public class CHUYENBAYsAPIController : ApiController
    {
        private HTQLBVMBver1 db = new HTQLBVMBver1();

        // GET: api/CHUYENBAYsAPI
        public IQueryable<CHUYENBAY> GetCHUYENBAYs()
        {
            return db.CHUYENBAYs;
        }

        // GET: api/CHUYENBAYsAPI/5
        [ResponseType(typeof(CHUYENBAY))]
        public IHttpActionResult GetCHUYENBAY(string id)
        {
            CHUYENBAY cHUYENBAY = db.CHUYENBAYs.Find(id);
            if (cHUYENBAY == null)
            {
                return NotFound();
            }

            return Ok(cHUYENBAY);
        }

        // PUT: api/CHUYENBAYsAPI/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCHUYENBAY(string id, CHUYENBAY cHUYENBAY)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cHUYENBAY.MaChuyenBay)
            {
                return BadRequest();
            }

            db.Entry(cHUYENBAY).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CHUYENBAYExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CHUYENBAYsAPI
        [ResponseType(typeof(CHUYENBAY))]
        public IHttpActionResult PostCHUYENBAY(CHUYENBAY cHUYENBAY)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CHUYENBAYs.Add(cHUYENBAY);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (CHUYENBAYExists(cHUYENBAY.MaChuyenBay))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = cHUYENBAY.MaChuyenBay }, cHUYENBAY);
        }

        // DELETE: api/CHUYENBAYsAPI/5
        [ResponseType(typeof(CHUYENBAY))]
        public IHttpActionResult DeleteCHUYENBAY(string id)
        {
            CHUYENBAY cHUYENBAY = db.CHUYENBAYs.Find(id);
            if (cHUYENBAY == null)
            {
                return NotFound();
            }

            db.CHUYENBAYs.Remove(cHUYENBAY);
            db.SaveChanges();

            return Ok(cHUYENBAY);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CHUYENBAYExists(string id)
        {
            return db.CHUYENBAYs.Count(e => e.MaChuyenBay == id) > 0;
        }
    }
}