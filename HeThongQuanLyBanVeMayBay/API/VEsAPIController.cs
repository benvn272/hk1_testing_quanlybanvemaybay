﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using HeThongQuanLyBanVeMayBay.Models;

namespace HeThongQuanLyBanVeMayBay.API
{
    public class VEsAPIController : ApiController
    {
        private HTQLBVMBver1 db = new HTQLBVMBver1();

        // GET: api/VEsAPI
        public IQueryable<VE> GetVEs()
        {
            return db.VEs;
        }

        // GET: api/VEsAPI/5
        [ResponseType(typeof(VE))]
        public IHttpActionResult GetVE(int id)
        {
            VE vE = db.VEs.Find(id);
            if (vE == null)
            {
                return NotFound();
            }

            return Ok(vE);
        }

        // PUT: api/VEsAPI/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutVE(int id, VE vE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != vE.MaVe)
            {
                return BadRequest();
            }

            db.Entry(vE).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VEExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/VEsAPI
        [ResponseType(typeof(VE))]
        public IHttpActionResult PostVE(VE vE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.VEs.Add(vE);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (VEExists(vE.MaVe))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = vE.MaVe }, vE);
        }

        // DELETE: api/VEsAPI/5
        [ResponseType(typeof(VE))]
        public IHttpActionResult DeleteVE(int id)
        {
            VE vE = db.VEs.Find(id);
            if (vE == null)
            {
                return NotFound();
            }

            db.VEs.Remove(vE);
            db.SaveChanges();

            return Ok(vE);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool VEExists(int id)
        {
            return db.VEs.Count(e => e.MaVe == id) > 0;
        }
    }
}