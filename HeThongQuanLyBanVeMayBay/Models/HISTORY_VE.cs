namespace HeThongQuanLyBanVeMayBay.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HISTORY_VE
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MaVe { get; set; }

        [StringLength(6)]
        public string MaChuyenBay { get; set; }

        [StringLength(3)]
        public string MaLoaiVe { get; set; }

        public int GiaVe { get; set; }

        public bool? TrangThai { get; set; }

        public bool? DeleteFlag { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DeleteTime { get; set; }

        public virtual CHUYENBAY CHUYENBAY { get; set; }

        public virtual LOAIVE LOAIVE { get; set; }
    }
}
