﻿namespace HeThongQuanLyBanVeMayBay.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("VE")]
    public partial class VE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public VE()
        {
            CT_DDV = new HashSet<CT_DDV>();
            HISTORY_CT_DDV = new HashSet<HISTORY_CT_DDV>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name ="Mã vé")]
        [Required(ErrorMessage ="Không được bỏ trống")]
        public int MaVe { get; set; }

        [StringLength(6)]
        [Display(Name ="Mã chuyến bay")]
        [Required(ErrorMessage = "Không được bỏ trống")]
        public string MaChuyenBay { get; set; }

        [StringLength(3)]
        [Display(Name ="Loại vé")]
        public string MaLoaiVe { get; set; }

        [Display(Name ="Số lượng")]
        [Required(ErrorMessage = "Không được bỏ trống")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Yêu cầu nhập số")]
        public int SoLuong { get; set; }

        [RegularExpression("^[0-9]*$", ErrorMessage = "Yêu cầu nhập số")]
        [Display(Name = "Giá vé")]
        [Required(ErrorMessage = "Không được bỏ trống")]
        public int GiaVe { get; set; }

        [Display(Name ="Trạng thái")]
        public bool? TrangThai { get; set; }

        public bool? DeleteFlag { get; set; }

        [Column(TypeName = "date")]
        [Display(Name ="Thời gian xóa")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? DeleteTime { get; set; }

        public virtual CHUYENBAY CHUYENBAY { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CT_DDV> CT_DDV { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HISTORY_CT_DDV> HISTORY_CT_DDV { get; set; }

        public virtual LOAIVE LOAIVE { get; set; }
    }
}
