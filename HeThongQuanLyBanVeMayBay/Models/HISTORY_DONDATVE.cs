namespace HeThongQuanLyBanVeMayBay.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HISTORY_DONDATVE
    {
        [Key]
        public int MaDDV { get; set; }

        public int? MaKH { get; set; }

        [StringLength(6)]
        public string MaKM { get; set; }

        public DateTime? NgayMuaVe { get; set; }

        public int? SoLuongDat { get; set; }

        public decimal? ThanhTien { get; set; }

        public bool? DaThanhToan { get; set; }

        public bool? DeleteFlag { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DeleteTime { get; set; }

        public virtual KHACHHANG KHACHHANG { get; set; }

        public virtual KHUYENMAI KHUYENMAI { get; set; }
    }
}
