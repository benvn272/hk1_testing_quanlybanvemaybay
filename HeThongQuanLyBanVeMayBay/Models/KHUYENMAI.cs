﻿namespace HeThongQuanLyBanVeMayBay.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("KHUYENMAI")]
    public partial class KHUYENMAI
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public KHUYENMAI()
        {
            DONDATVEs = new HashSet<DONDATVE>();
            HISTORY_DONDATVE = new HashSet<HISTORY_DONDATVE>();
        }

        [Key]
        [StringLength(6)]
        [Display(Name ="Mã khuyến mãi")]
        public string MaKM { get; set; }

        [Column("KhuyenMai")]
        public int? KhuyenMai1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DONDATVE> DONDATVEs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HISTORY_DONDATVE> HISTORY_DONDATVE { get; set; }
    }
}
