namespace HeThongQuanLyBanVeMayBay.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HISTORY_KHACHHANG
    {
        [Key]
        public int MaKH { get; set; }

        [StringLength(50)]
        public string TenKH { get; set; }

        [StringLength(50)]
        public string TenDangNhap { get; set; }

        [StringLength(50)]
        public string MatKhau { get; set; }

        [Column(TypeName = "date")]
        public DateTime? NgaySinh { get; set; }

        [StringLength(12)]
        public string SDT { get; set; }

        [StringLength(12)]
        public string CMND { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        public bool? DeleteFlag { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DeleteTime { get; set; }
    }
}
