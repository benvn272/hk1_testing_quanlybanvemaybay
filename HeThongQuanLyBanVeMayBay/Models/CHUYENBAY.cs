﻿namespace HeThongQuanLyBanVeMayBay.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CHUYENBAY")]
    public partial class CHUYENBAY
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CHUYENBAY()
        {
            HISTORY_VE = new HashSet<HISTORY_VE>();
            VEs = new HashSet<VE>();
        }

        [Key]
        [StringLength(6)]
        [Display(Name ="Mã chuyến bay")]
        [Required(ErrorMessage = "Không được bỏ trống")]
        public string MaChuyenBay { get; set; }

        [StringLength(30)]
        [Display(Name = "Sân bay đi")]
        [Required(ErrorMessage = "Không được bỏ trống")]
        public string SanBayDi { get; set; }

        [StringLength(30)]
        [Display(Name ="Sân bay đến")]
        [Required(ErrorMessage = "Không được bỏ trống")]
        public string SanBayDen { get; set; }

        [Display(Name = "Thời gian bay")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ThoiGianBay { get; set; }

        [Display(Name ="Thời gian đến")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ThoiGianDen { get; set; }

        [Display(Name ="Tình trạng")]
        public bool? TrinhTrang { get; set; }

        [Display(Name ="Số ghế người lớn")]
        [RegularExpression("^[0-9]*$", ErrorMessage ="Yêu cầu số")]
        public int SoGheNguoiLon { get; set; }

        [Display(Name = "Số ghế trẻ em")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Yêu cầu số")]
        public int? SoGheTreEm { get; set; }

        [Display(Name = "Số ghế em bé")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Yêu cầu số")]
        public int? SoGheEmBe { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HISTORY_VE> HISTORY_VE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VE> VEs { get; set; }
    }
}
