namespace HeThongQuanLyBanVeMayBay.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class HTQLBVMBver1 : DbContext
    {
        public HTQLBVMBver1()
            : base("name=HTQLBVMBver1")
        {
        }

        public virtual DbSet<BANGRANGBUOC> BANGRANGBUOCs { get; set; }
        public virtual DbSet<CHUCVU> CHUCVUs { get; set; }
        public virtual DbSet<CHUYENBAY> CHUYENBAYs { get; set; }
        public virtual DbSet<CT_DDV> CT_DDV { get; set; }
        public virtual DbSet<DONDATVE> DONDATVEs { get; set; }
        public virtual DbSet<HISTORY_CT_DDV> HISTORY_CT_DDV { get; set; }
        public virtual DbSet<HISTORY_DONDATVE> HISTORY_DONDATVE { get; set; }
        public virtual DbSet<HISTORY_KHACHHANG> HISTORY_KHACHHANG { get; set; }
        public virtual DbSet<HISTORY_VE> HISTORY_VE { get; set; }
        public virtual DbSet<KHACHHANG> KHACHHANGs { get; set; }
        public virtual DbSet<KHUYENMAI> KHUYENMAIs { get; set; }
        public virtual DbSet<LOAIVE> LOAIVEs { get; set; }
        public virtual DbSet<NHANVIEN> NHANVIENs { get; set; }
        public virtual DbSet<VE> VEs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BANGRANGBUOC>()
                .Property(e => e.MaRB)
                .IsUnicode(false);

            modelBuilder.Entity<CHUCVU>()
                .Property(e => e.MaChucVu)
                .IsUnicode(false);

            modelBuilder.Entity<CHUYENBAY>()
                .Property(e => e.MaChuyenBay)
                .IsUnicode(false);

            modelBuilder.Entity<CT_DDV>()
                .Property(e => e.GiaVe)
                .HasPrecision(18, 0);

            modelBuilder.Entity<DONDATVE>()
                .Property(e => e.MaKM)
                .IsUnicode(false);

            modelBuilder.Entity<DONDATVE>()
                .Property(e => e.ThanhTien)
                .HasPrecision(18, 0);

            modelBuilder.Entity<DONDATVE>()
                .HasMany(e => e.CT_DDV)
                .WithRequired(e => e.DONDATVE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DONDATVE>()
                .HasMany(e => e.HISTORY_CT_DDV)
                .WithRequired(e => e.DONDATVE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HISTORY_CT_DDV>()
                .Property(e => e.GiaVe)
                .HasPrecision(18, 0);

            modelBuilder.Entity<HISTORY_DONDATVE>()
                .Property(e => e.MaKM)
                .IsUnicode(false);

            modelBuilder.Entity<HISTORY_DONDATVE>()
                .Property(e => e.ThanhTien)
                .HasPrecision(18, 0);

            modelBuilder.Entity<HISTORY_KHACHHANG>()
                .Property(e => e.TenDangNhap)
                .IsUnicode(false);

            modelBuilder.Entity<HISTORY_KHACHHANG>()
                .Property(e => e.MatKhau)
                .IsUnicode(false);

            modelBuilder.Entity<HISTORY_KHACHHANG>()
                .Property(e => e.SDT)
                .IsUnicode(false);

            modelBuilder.Entity<HISTORY_KHACHHANG>()
                .Property(e => e.CMND)
                .IsUnicode(false);

            modelBuilder.Entity<HISTORY_KHACHHANG>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<HISTORY_VE>()
                .Property(e => e.MaChuyenBay)
                .IsUnicode(false);

            modelBuilder.Entity<HISTORY_VE>()
                .Property(e => e.MaLoaiVe)
                .IsUnicode(false);

            modelBuilder.Entity<KHACHHANG>()
                .Property(e => e.TenDangNhap)
                .IsUnicode(false);

            modelBuilder.Entity<KHACHHANG>()
                .Property(e => e.MatKhau)
                .IsUnicode(false);

            modelBuilder.Entity<KHACHHANG>()
                .Property(e => e.SDT)
                .IsUnicode(false);

            modelBuilder.Entity<KHACHHANG>()
                .Property(e => e.CMND)
                .IsUnicode(false);

            modelBuilder.Entity<KHACHHANG>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<KHUYENMAI>()
                .Property(e => e.MaKM)
                .IsUnicode(false);

            modelBuilder.Entity<LOAIVE>()
                .Property(e => e.MaLoaiVe)
                .IsUnicode(false);

            modelBuilder.Entity<NHANVIEN>()
                .Property(e => e.MaChucVu)
                .IsUnicode(false);

            modelBuilder.Entity<NHANVIEN>()
                .Property(e => e.TenDangNhap)
                .IsUnicode(false);

            modelBuilder.Entity<NHANVIEN>()
                .Property(e => e.MatKhau)
                .IsUnicode(false);

            modelBuilder.Entity<NHANVIEN>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<NHANVIEN>()
                .Property(e => e.SDT)
                .IsUnicode(false);

            modelBuilder.Entity<VE>()
                .Property(e => e.MaChuyenBay)
                .IsUnicode(false);

            modelBuilder.Entity<VE>()
                .Property(e => e.MaLoaiVe)
                .IsUnicode(false);

            modelBuilder.Entity<VE>()
                .HasMany(e => e.CT_DDV)
                .WithRequired(e => e.VE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VE>()
                .HasMany(e => e.HISTORY_CT_DDV)
                .WithRequired(e => e.VE)
                .WillCascadeOnDelete(false);
        }
    }
}
