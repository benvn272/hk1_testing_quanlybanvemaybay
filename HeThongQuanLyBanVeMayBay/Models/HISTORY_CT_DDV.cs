namespace HeThongQuanLyBanVeMayBay.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HISTORY_CT_DDV
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MaDDV { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MaVe { get; set; }

        public decimal? GiaVe { get; set; }

        public bool? DeleteFlag { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DeleteTime { get; set; }

        public virtual DONDATVE DONDATVE { get; set; }

        public virtual VE VE { get; set; }
    }
}
