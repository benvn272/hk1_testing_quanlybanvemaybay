﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HeThongQuanLyBanVeMayBay.Models;

namespace HeThongQuanLyBanVeMayBay.Models
{
    public class Giohang
    {
        HTQLBVMBver1 data = new HTQLBVMBver1();
        public int MaVe { get; set; }

        public string MaChuyenBay { get; set; }

        public Double GiaVe { get; set; }

        
        public int SoLuong { get; set; }

        public Double dThanhTien
        {
            get { return SoLuong * GiaVe; }
        }

        public Giohang(int iMaVe)
        {
            MaVe = iMaVe;
            VE phukien = data.VEs.Single(n => n.MaVe == MaVe);
            MaChuyenBay = phukien.MaChuyenBay;
            GiaVe = double.Parse(phukien.GiaVe.ToString());
            SoLuong = 1;
        }
    }
}