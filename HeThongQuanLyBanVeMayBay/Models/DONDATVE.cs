﻿namespace HeThongQuanLyBanVeMayBay.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DONDATVE")]
    public partial class DONDATVE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DONDATVE()
        {
            CT_DDV = new HashSet<CT_DDV>();
            HISTORY_CT_DDV = new HashSet<HISTORY_CT_DDV>();
        }

        [Key]
        [Display(Name ="Đơn đặt vé")]
        public int MaDDV { get; set; }

        [Display(Name = "Mã khách hàng")]
        public int? MaKH { get; set; }

        [StringLength(6)]
        [Display(Name ="Mã khuyến mãi")]
        public string MaKM { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Ngày mua")]
        public DateTime? NgayMuaVe { get; set; }


        [Display(Name ="Thành tiền")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Yêu cầu số")]
        public decimal? ThanhTien { get; set; }

        
        public bool? DeleteFlag { get; set; }

        [Column(TypeName = "date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name ="Thời gian xóa")]
        public DateTime? DeleteTime { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CT_DDV> CT_DDV { get; set; }

        public virtual KHACHHANG KHACHHANG { get; set; }

        public virtual KHUYENMAI KHUYENMAI { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HISTORY_CT_DDV> HISTORY_CT_DDV { get; set; }
    }
}
