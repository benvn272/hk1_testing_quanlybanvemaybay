﻿namespace HeThongQuanLyBanVeMayBay.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("NHANVIEN")]
    public partial class NHANVIEN
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Mã nhân viên")]
        [Required(ErrorMessage = "Không được bỏ trống")]
        public int MaNV { get; set; }

        [StringLength(3)]
        [Display(Name = "Mã chức vụ")]
        public string MaChucVu { get; set; }

        [Required(ErrorMessage = "Yêu cầu họ tên")]
        [StringLength(50)]
        [Display(Name = "Họ tên")]
        public string TenNV { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage ="Yêu cầu tên đăng nhập")]
        [Display(Name = "Tên đăng nhập")]
        public string TenDangNhap { get; set; }

        [Required(ErrorMessage ="Yêu cầu mật khẩu")]
        [StringLength(50)]
        [Display(Name = "Mật khẩu")]
        [MinLength(3, ErrorMessage = "Mật khẩu tối thiểu 3 ký tự")]
        [DataType(DataType.Password)]
        public string MatKhau { get; set; }

        [Column(TypeName = "date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode =true)]
        [Display(Name ="Ngày sinh")]
        public DateTime? NgaySinh { get; set; }

        [StringLength(50)]
        [Display(Name ="Địa chỉ")]
        public string DiaChi { get; set; }

        [StringLength(50)]
        [Display(Name ="Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [StringLength(12)]
        public string SDT { get; set; }

        public virtual CHUCVU CHUCVU { get; set; }
    }
}
