namespace HeThongQuanLyBanVeMayBay.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BANGRANGBUOC")]
    public partial class BANGRANGBUOC
    {
        [Key]
        [StringLength(6)]
        public string MaRB { get; set; }

        public int GiaTri { get; set; }

        [Required]
        [StringLength(100)]
        public string GhiChu { get; set; }
    }
}
