﻿namespace HeThongQuanLyBanVeMayBay.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("KHACHHANG")]
    public partial class KHACHHANG
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public KHACHHANG()
        {
            DONDATVEs = new HashSet<DONDATVE>();
            HISTORY_DONDATVE = new HashSet<HISTORY_DONDATVE>();
        }

        [Key]
        [Display(Name = "Mã khách hàng")]
        public int MaKH { get; set; }

        [StringLength(50)]
        [Display(Name ="Họ tên")]
        [Required(ErrorMessage ="Yêu cầu tên khách hàng")]
        public string TenKH { get; set; }

        [Required(ErrorMessage = "Yêu cầu tên đăng nhập")]
        [StringLength(50)]
        [Display(Name = "Tên đăng nhập")]
        public string TenDangNhap { get; set; }

        [StringLength(50)]
        [Display(Name = "Mật khẩu")]
        [MinLength(4, ErrorMessage = "Mật khẩu ít nhất 4 ký tự")]
        [DataType(DataType.Password)]
        public string MatKhau { get; set; }

        [Column(TypeName = "date")]
        [Display(Name = "Ngày sinh")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? NgaySinh { get; set; }

        [StringLength(12)]
        [Display(Name ="SĐT")]
        [MinLength(10, ErrorMessage = "SĐT tối thiểu 10 số")]
        [MaxLength(12, ErrorMessage = "SĐT tối đa 12 số")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Yêu cầu nhập số")]
        public string SDT { get; set; }

        [StringLength(12)]
        [Display(Name = "CMND")]
        [MinLength(12, ErrorMessage = "CMND tối thiểu 12 số")]
        [MaxLength(12, ErrorMessage = "CMND tối đa 12 số")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Yêu cầu nhập số")]
        public string CMND { get; set; }

        [StringLength(50)]
        [Display(Name = "Email")]
        public string Email { get; set; }

        public bool? DeleteFlag { get; set; }

        [Column(TypeName = "date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? DeleteTime { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DONDATVE> DONDATVEs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HISTORY_DONDATVE> HISTORY_DONDATVE { get; set; }
    }
}
