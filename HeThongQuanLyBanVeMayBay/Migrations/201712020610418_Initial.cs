namespace HeThongQuanLyBanVeMayBay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CT_DDV", "SoLuongDat", c => c.Int(nullable: false));
            AddColumn("dbo.CT_DDV", "DaThanhToan", c => c.Int(nullable: false));
            DropColumn("dbo.DONDATVE", "SoLuongDat");
            DropColumn("dbo.DONDATVE", "DaThanhToan");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DONDATVE", "DaThanhToan", c => c.Int(nullable: false));
            AddColumn("dbo.DONDATVE", "SoLuongDat", c => c.Int(nullable: false));
            DropColumn("dbo.CT_DDV", "DaThanhToan");
            DropColumn("dbo.CT_DDV", "SoLuongDat");
        }
    }
}
